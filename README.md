# **Jutge ITB**

### **Descripció del projecte**
Aquest projecte podràs posar a prova les teves habilitats de programació. El programa us presentarà un problema a través de la consola. El teu proposit,
trobar la solució.



### **Instal·lació i execució del projecte**
Primer de tot anem a internet i descàrreguem **intellij idea**.

Git clone a la terminal de linux per poder executar el programa en aquesta versió:

```sh
$ git clone https://gitlab.com/gerard.sanchez.7e6/jutgitb.git
```

Descarregar, descomprimir y exportar l'arxiu en format zip del Servidor PostgresSQL.

Executa **startSql.sh** 

Obrir el servidor 
```sh
$ psql postgres 
```
Connectar a la BD 
```sh
$ \c jutge
```

Per últim, obre **Intellj**  i esull el programa per poder-lo executar 

Executa aquest arxiu:
>Main.kt



### **Autors**
- Gerad Sanchez ([Gitlab](https://gitlab.com/gerard.sanchez.7e6))


### **License**
Visualitza la llicència [License](LICENSE)

