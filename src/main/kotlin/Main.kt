import org.postgresql.util.PSQLException
import java.sql.Connection
import java.sql.DriverManager
import kotlin.system.exitProcess

/**
 * @autor Gerard Sanchez Preto
 * @version 1.3
 */

const val jdbclURL = "jdbc:postgresql://localhost:5432/jutge"
var llistaProblemes = mutableListOf<Problema>()

/**
 * Funció que s'encarrega de fer el títol del programa.
 * @return String del títol.
 */
fun titleProgram() :String{
    return "    ___       _          _____ ___________ \n" +
            "  |_  |     | |        |_   _|_   _| ___ \\\n" +
            "    | |_   _| |_ __ _    | |   | | | |_/ /\n" +
            "    | | | | | __/ _` |   | |   | | | ___ \\\n" +
            "/\\__/ / |_| | || (_| |  _| |_  | | | |_/ /\n" +
            "\\____/ \\__,_|\\__\\__, |  \\___/  \\_/ \\____/ \n" +
            "                 __/ |                    \n" +
            "                |___/                     \n"
}

/**
 * Funció que s'encarrega de fer el menú per identificar l'usuari.
 * @return String del rol del usuari.
 */
fun printUserRol() : String{
    return  "--- IDENTIFICA'T ---\n" +
            "1 -> Sóc alumne\n" +
            "2 -> Sóc professor\n" +
            "--------------------\n" +
            "Opció : "
}

/**
 * Funció que s'encarregar de generar el menú del alumne i segons l'opció escollida, farà una
 * acció o una altre.
 */
fun studentOptions(connection: Connection) {
    print("\n---------------- MENÚ ALUMNE ----------------\n" +
            "1 -> Seguir amb l'itinerari d'aprenentatge.\n" +
            "2 -> Llista de problemes.\n" +
            "3 -> Consultar històtic de problemes resolts.\n" +
            "4 -> Ajuda\n" +
            "5 -> Sortir\n" +
            "---------------------------------------------\n" +
            "Opció : "
    )
    do {
        val studentOption = readln()
        when (studentOption) {
            "1" -> { seguirAmbItinerariAprenentatge(connection) }
            "2" -> { listOfProblems(connection) }
            "3" -> { studentHistory(connection) }
            "4" -> { printStudentHelp(connection) }
            "5" -> { exitProcess(0) }
            else ->{ println("Error")
            studentOptions(connection)}
        }
    } while (studentOption.toInt() !in 1..5)
}

/**
 * Funció que s'encarrega de fer el Login del rol de professor.
 */
fun teacherLogIn(connection: Connection) {
    do {
        print("Contrasenya : ")
        val password = readln()

        if (password == "ITB2023") {
            teacherOptions(connection) }
        else { println("Error\n") }
    } while (password != "ITB2023" )

}

/**
 * Funció que s'encarregar de generar el menú del professor i segons l'opció escollida, farà una
 * acció o una altre.
 */
fun teacherOptions(connection: Connection) {
    print("\n--------- MENÚ PROFESSOR ---------\n" +
            "1 -> Afegir nous problemes\n" +
            "2 -> Treure un report de l'alumne\n" +
            "3 -> Sortir\n" +
            "----------------------------------\n" +
            "Opció : "
    )
    do {
        val teacherOption = readln()
        when (teacherOption) {
            "1" -> { addProblems(connection) }
            "2" -> { addMark() }
            "3" -> { exitProcess(0) }
            else -> { println("Error")
            teacherOptions(connection) }
        }
    } while (teacherOption.toInt() !in 1..3)
}

/**
 * Funció que s'encarrega de mirar si el problema està resolt, si no ho està,
 */
fun seguirAmbItinerariAprenentatge(connection: Connection) {
    for (problema in llistaProblemes) {
        if (!problema.resolt) {
            problema.solveProblem(connection)
        }
    }
}

/**
 * Funció que s'encarrega de imprimir la llista on estan tots els problemes, on l'usuari pot
 * escollir quin resoldre.
 */
fun listOfProblems(connection: Connection) {
    var problemId = 1
    println("\n-------- PROBLEMES --------")
    for (problema in llistaProblemes) {
        println("$problemId -> ${problema.title}")
        problemId++
    }
    println("---------------------------")
    do {
        print("Escull una opció o MENU : ")
        val studentChoose = readln().uppercase()

        if (studentChoose == "MENU"){
            studentOptions(connection) }
        if (studentChoose.toInt() !in 1..llistaProblemes.size) {
            println("Problema escollit inexistent\n") }
        else{
            val problema = llistaProblemes[studentChoose.toInt()-1]
            problema.solveProblem(connection) }
    } while (studentChoose.toInt() !in 1..llistaProblemes.size)
    studentOptions(connection)
}

/**
 * Funció que s'encarrega de fer un petit historial dels problemes resolts del alumne.
 */
fun studentHistory(connection: Connection) {
    for (problema in llistaProblemes) {
        if (problema.resolt){
            println("\n${problema.problemId}-> ${problema.title}\n" +
                    "INTENTS -> ${problema.llistaIntents}\n" +
                    "ESTAT-> RESOLT")
        }

    }
    studentOptions(connection )

}

/**
 * Funció que s'encarrega d' imprimir un petit resum del que pots arribar a fer en el programa, nomes
 * del alumne
 */
fun printStudentHelp(connection: Connection) {
    print("\nBenvingut al JUTG ITB 2023 alumne.\n" +
            "Seguir amb l'itinerari d'aprenentage -> Continuar resolent els problemes per on ho vas deixar l'últim cop.\n" +
            "Llistat de problemes -> Escollir el problema que vols resoldre.\n" +
            "Consultar històtic de problemes resolts -> Historial dels problemes resolts.\n" +
            "Sortir -> Sortir del programa.\n")
    studentOptions(connection)

}

/**
 * Funció que s'encarrega de crear nous problemes i seguidament afegir-los a la BD amb tots els altres.
 */
fun addProblems(connection: Connection) {
    try{
        val lastIndex = llistaProblemes.size + 1
        val sqlTaulaProblema = "INSERT INTO problema (title, sentence, resolt, intents) VALUES (?, ?, ?, ?)"
        val sqlTaulaJocDeProvesPublic = "INSERT INTO jocdeprovespublic (id_problema, input, output) VALUES (?, ?, ?)"
        val sqlTaulaJocDeProvesPrivat = "INSERT INTO jocdeprovesprivat (id_problema, input, output) VALUES (?, ?, ?)"

        println()
        print("TITOL: ")
        val title = readln()
        print("ENUNCIAT: ")
        val sentence= readln()
        print("INPUT PÚBLIC: ")
        val publicInput =   readln()
        print("OUTPUT PÚBLIC: ")
        val publicOutput = readln()
        print("INPUT PRIVAT: ")
        val privateInput = readln()
        print("OUTPUT PRIVAT: ")
        val privateOutput = readln()

        llistaProblemes.add(Problema(lastIndex,title, sentence, JocDeProves(publicInput, publicOutput), JocDeProves(privateInput, privateOutput), 0, mutableListOf(),false))
        println("Problema afegit")

        val taulaProblemaStatement = connection.prepareStatement(sqlTaulaProblema)
        taulaProblemaStatement.setString(1, title)
        taulaProblemaStatement.setString(2, sentence)
        taulaProblemaStatement.setBoolean(3, false)
        taulaProblemaStatement.setInt(4, 0)
        taulaProblemaStatement.executeUpdate()

        val taulaJocDeProvesPublicStatement = connection.prepareStatement(sqlTaulaJocDeProvesPublic)
        taulaJocDeProvesPublicStatement.setInt(1, lastIndex)
        taulaJocDeProvesPublicStatement.setString(2, publicInput)
        taulaJocDeProvesPublicStatement.setString(3, publicOutput)
        taulaJocDeProvesPublicStatement.executeUpdate()

        val taulaJocDeProvesPrivatStatement = connection.prepareStatement(sqlTaulaJocDeProvesPrivat)
        taulaJocDeProvesPrivatStatement.setInt(1, lastIndex)
        taulaJocDeProvesPrivatStatement.setString(2, privateInput)
        taulaJocDeProvesPrivatStatement.setString(3, privateOutput)
        taulaJocDeProvesPrivatStatement.executeUpdate()

    } catch (e: PSQLException) {
        println("Error $e")
    }
    teacherOptions(connection)
}

/**
 * Funció que s'encarrega de generar una nota al usuari segons el total dels seus intents.
 */
fun addMark() {
    for (problema in llistaProblemes) {
        if (problema.resolt) {
            println(
                "\n${problema.problemId} -> ${problema.title}\n" +
                        "INTENTS -> ${problema.intents}"
            )

            if (problema.intents in 1..11) {
                println("NOTA -> ${11 - problema.intents}")
            } else {
                println("NOTA -> 0")
            }
        }
    }
}

/**
 * Funció que s'encarrega d'afegir la resposta de l'usuari del problema  a la BD.
 */
fun addIntentBd(connection: Connection, problemId : Int, userAnswer : String){
    try{
        val queryAddIntent = "INSERT INTO intent (id_problema, answer) VALUES (?, ?)"
        val preparedStatementIntent = connection.prepareStatement(queryAddIntent)
        preparedStatementIntent.setInt(1, problemId)
        preparedStatementIntent.setString(2, userAnswer)

        preparedStatementIntent.executeUpdate()

    } catch (e: PSQLException){
        println("Error $e")
    }
}

/**
 * Funció que s'encarrega de modificar l'estat del problema segons els intents realitzats per l'usuari i si aquest
 * l'ha resolt o no.
 */
fun updateProblemBd(connection: Connection, title : String, sentence : String, resolt : Boolean, intents : Int, problemId: Int){
    try{
        val queryUpdateProblem = "UPDATE problema SET  title = ?, sentence = ?, resolt = ?, intents = ? WHERE id_problema = ?"
        val preparedStatementProblema = connection.prepareStatement(queryUpdateProblem)

        preparedStatementProblema.setString(1, title)
        preparedStatementProblema.setString(2, sentence)
        preparedStatementProblema.setBoolean(3, resolt)
        preparedStatementProblema.setInt(4, intents)
        preparedStatementProblema.setInt(5, problemId)

        preparedStatementProblema.executeUpdate()

    } catch (e: PSQLException){
        println("Error $e")
    }
}

/**
 * Funció que s'encarrega de traspassar els problemes de la BD a una llista per facilitar la gestió de Dades.
 */
fun readBdProblems(connection : Connection) {
    try {
        var enunciatProblema: String
        var idProblema: Int
        var problemaResolt: Boolean
        var intents: Int
        var title: String
        var jocDeProvesPublic = JocDeProves("", "")
        var jocDeProvesPrivat = JocDeProves("", "")


        val queryProblema = "SELECT * FROM problema"
        val queryJocDeProvesPublic = "SELECT * FROM jocdeprovespublic WHERE id_problema = ?"
        val queryJocDeProvesPrivat = "SELECT * FROM jocdeprovesprivat WHERE id_problema = ?"
        val queryIntents = "SELECT * FROM intent WHERE id_problema = ?"

        connection.prepareStatement(queryProblema).use { statementProblema ->
            val resultProblema = statementProblema.executeQuery()

            while (resultProblema.next()) {
                idProblema = resultProblema.getInt("id_problema")
                title = resultProblema.getString("title")
                enunciatProblema = resultProblema.getString("sentence")
                problemaResolt = resultProblema.getBoolean("resolt")
                intents = resultProblema.getInt("intents")

                connection.prepareStatement(queryJocDeProvesPublic).use { statementJocDeProvesPublic ->
                    statementJocDeProvesPublic.setInt(1, idProblema)
                    val resultJocDeProvesPublic = statementJocDeProvesPublic.executeQuery()

                    while (resultJocDeProvesPublic.next()) {
                        val jocDeProvesPublicInput = resultJocDeProvesPublic.getString("input")
                        val jocDeProvesPublicOutput = resultJocDeProvesPublic.getString("output")
                        jocDeProvesPublic = JocDeProves(jocDeProvesPublicInput, jocDeProvesPublicOutput)
                    }
                }
                connection.prepareStatement(queryJocDeProvesPrivat).use { statementJocDeProvesPrivat ->
                    statementJocDeProvesPrivat.setInt(1, idProblema)
                    val resultJocDeProvesPrivat = statementJocDeProvesPrivat.executeQuery()

                    while (resultJocDeProvesPrivat.next()) {
                        val jocDeProvesPrivatInput = resultJocDeProvesPrivat.getString("input")
                        val jocDeProvesPrivatOutput = resultJocDeProvesPrivat.getString("output")
                        jocDeProvesPrivat = JocDeProves(jocDeProvesPrivatInput, jocDeProvesPrivatOutput)
                    }
                }
                connection.prepareStatement(queryIntents).use { statementIntent ->
                    statementIntent.setInt(1, idProblema)
                    val resultIntent = statementIntent.executeQuery()
                    val llistaIntents = mutableListOf<Intent>()

                    while (resultIntent.next()) {
                        val intentRespota = resultIntent.getString("answer")
                        llistaIntents.add(Intent(intentRespota))
                    }

                    val problema = Problema(
                        idProblema,
                        title,
                        enunciatProblema,
                        jocDeProvesPublic,
                        jocDeProvesPrivat,
                        intents,
                        llistaIntents,
                        problemaResolt
                    )
                    llistaProblemes.add(problema)
                }
            }
        }
    }catch (e: PSQLException){
        println("Error $e")
    }
}

fun main() {
    try{
        val connection = DriverManager.getConnection(jdbclURL)

        println(titleProgram())
        readBdProblems(connection)

        do {
            print(printUserRol())
            val userRol = readln()
            when (userRol) {
                "1" -> {studentOptions(connection)}
                "2" -> {teacherLogIn(connection)}
                else ->{ println("Error\n") }
            }
        }while (userRol.toInt() !in 1..2)

        connection.close()

    }catch (e: PSQLException){
        println("Error $e")
    }
}