import java.sql.Connection
class Problema(val problemId : Int,
               val title: String,
               val sentence: String,
               val jocDeProvesPublic: JocDeProves,
               val jocDeProvesPrivat: JocDeProves,
               var intents: Int,
               var llistaIntents: MutableList<Intent>,
               var resolt: Boolean = false){

    /**
     * Funció que s'encarrega de mostrar el problema
     */
    private fun printInfoProblem(): String {
        return "\n--------------------------------------\n" +
                "$title \n" +
                "$sentence \n" +
                "INPUT: ${jocDeProvesPublic.input}\n" +
                "OUTPUT: ${jocDeProvesPublic.output}\n" +
                "--------------------------------------"
    }

    /**
     * Funció que s'encarrega de gestionar tot el problema segons el que l'usuari vulgui fer.
     */
    fun solveProblem(connection: Connection) {
        println(printInfoProblem())
        do {
            print("Resoldre el problema? SI / NO / MENU : ")
            val studentOption = readln().uppercase()
            when (studentOption) {
                "SI" -> {
                    do {
                        var userError = "NO"
                        print(
                            "\nINPUT: ${jocDeProvesPrivat.input}\n" +
                                    "Resposta : "
                        )
                        val userAnswer = readln()
                        intents++
                        llistaIntents.add(Intent(userAnswer))
                        addIntentBd(connection, problemId, userAnswer)

                        if (jocDeProvesPrivat.output == userAnswer) {
                            println("CORRECTE!!!")
                            resolt = true

                        } else {
                            println("INCORRECTE!!!\n")

                            print("Tornar-ho a itentar? SI / NO : ")
                            userError = readln().uppercase()
                        }
                        updateProblemBd(connection, title, sentence, resolt, intents, problemId)

                    } while (userError != "NO")
                }

                "MENU" -> {
                    studentOptions(connection)
                }
                else -> {
                    if (studentOption != "NO") {
                        println("Error\n")
                    }
                }
            }
        }while (studentOption != "NO" && studentOption != "SI" && studentOption != "MENU")
    }
}


